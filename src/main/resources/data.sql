INSERT INTO account(id, password, username)
VALUES (1, '$2y$12$pS5WaYWus1fANUn.BYFmtuPF6Fn1yt6cBOoFZ4AT.aIpKBcwdy7OG', 'admin'),
       (2, '$2y$12$69E2GZAaUX1ZlOioNZ0YQeVNS6h/1vfRy1uxwWBQNz4vFha0gcAYO', 'user');

INSERT INTO authority(id, name)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');

INSERT INTO account_authorities(account_id, authorities_id)
VALUES (1, 1),
       (1, 2),
       (2, 2);