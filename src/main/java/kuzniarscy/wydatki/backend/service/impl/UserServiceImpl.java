package kuzniarscy.wydatki.backend.service.impl;

import kuzniarscy.wydatki.backend.entity.Account;
import kuzniarscy.wydatki.backend.repository.AccountRepository;
import kuzniarscy.wydatki.backend.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final AccountRepository accountRepository;

    public UserServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Optional<Account> findUserById(Long id) {
        return accountRepository.findById(id);
    }
}
