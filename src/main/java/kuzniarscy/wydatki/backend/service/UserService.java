package kuzniarscy.wydatki.backend.service;

import kuzniarscy.wydatki.backend.entity.Account;

import java.util.Optional;

public interface UserService {
    Optional<Account> findUserById(Long id);
}
