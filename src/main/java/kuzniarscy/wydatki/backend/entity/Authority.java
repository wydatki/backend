package kuzniarscy.wydatki.backend.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Authority {

    @Id
    private long id;

    @Column(unique = true, nullable = false)
    private String name;
}
