package kuzniarscy.wydatki.backend.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    @org.springframework.data.annotation.Transient
    private String password;

    @ManyToMany(targetEntity = Authority.class, fetch = FetchType.EAGER)
    private Set<Authority> authorities;
}

