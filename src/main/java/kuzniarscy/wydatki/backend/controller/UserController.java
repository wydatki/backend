package kuzniarscy.wydatki.backend.controller;

import kuzniarscy.wydatki.backend.entity.Account;
import kuzniarscy.wydatki.backend.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public ResponseEntity<String> currentUser() {
        return ResponseEntity.ok("THIS USER");
    }

    @GetMapping("/{id}")
    public ResponseEntity anotherUser(@PathVariable Long id, @RequestParam String name) {
        try {
            return userService.findUserById(id)
                    .map(Account::getId)
                    .map(ResponseEntity::ok)
                    .orElse(ResponseEntity.notFound().build());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Bad data");
        }
    }

}
